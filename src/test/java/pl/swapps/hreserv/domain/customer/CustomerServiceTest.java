package pl.swapps.hreserv.domain.customer;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import pl.swapps.hreserv.exception.CustomerEmailAlreadyExistsException;

import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.is;

public class CustomerServiceTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private CustomerRepository customerRepository = Mockito.mock(CustomerRepository.class);

    private CustomerService customerService = new CustomerService(customerRepository);

    @Test
    public void shouldRegisterCustomerWithNormalizedEmail() {
        // given
        when(customerRepository.existsByEmailIgnoreCase(anyString())).thenReturn(false);
        when(customerRepository.save(any(Customer.class))).thenAnswer(i -> i.getArguments()[0]);

        // when
        customerService.registerCustomer("PJAKUBOw@GmAiL.com");

        // then
        ArgumentCaptor<Customer> argument = ArgumentCaptor.forClass(Customer.class);
        verify(customerRepository).save(argument.capture());
        assertThat(argument.getValue().getEmail(), is("pjakubow@gmail.com"));
    }

    @Test
    public void shouldThrowWhenCustomerEmailAlreadyExists() {
        // given
        when(customerRepository.existsByEmailIgnoreCase(anyString())).thenReturn(true);

        // expect
        exception.expect(CustomerEmailAlreadyExistsException.class);
        customerService.registerCustomer("pjakubow@gmail.com");
    }
}