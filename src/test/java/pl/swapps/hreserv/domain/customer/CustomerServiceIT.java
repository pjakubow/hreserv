package pl.swapps.hreserv.domain.customer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import pl.swapps.hreserv.domain.customer.Customer;
import pl.swapps.hreserv.domain.customer.CustomerRepository;
import pl.swapps.hreserv.domain.customer.CustomerService;
import pl.swapps.hreserv.domain.customer.CustomerDto;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class CustomerServiceIT {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    public void shouldRegisterCustomer() {
        // given
        String testEmail = "test-email@test-server.com";

        // when
        CustomerDto customerDto = customerService.registerCustomer(testEmail);

        // then
        assertThat(customerDto.getId(), notNullValue());
        String actualEmail = customerRepository.findById(customerDto.getId()).map(Customer::getEmail).orElse("");
        assertThat(actualEmail, equalTo(testEmail));
    }
}