package pl.swapps.hreserv.domain.room;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import pl.swapps.hreserv.domain.customer.Customer;
import pl.swapps.hreserv.domain.customer.CustomerRepository;
import pl.swapps.hreserv.domain.hotel.Hotel;
import pl.swapps.hreserv.domain.hotel.HotelRepository;
import pl.swapps.hreserv.domain.reservation.Reservation;
import pl.swapps.hreserv.domain.reservation.ReservationRepository;
import pl.swapps.hreserv.domain.reservation.ReservationService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class RoomServiceAvailableRoomsIT {

    private static final String city = "Test City";
    private static final String email = "pjakubow@gmail.com";
    private static final LocalDate dateFrom = LocalDate.parse("2018-09-03");
    private static final LocalDate dateTo = LocalDate.parse("2018-09-05");

    @Autowired
    private RoomService roomService;

    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private ReservationService reservationService;

    @Before
    public void setup() {
        reservationRepository.deleteAll();
        customerRepository.deleteAll();
        roomRepository.deleteAll();
        hotelRepository.deleteAll();
    }

    @After
    public void tearDown() {
        reservationRepository.deleteAll();
        customerRepository.deleteAll();
        roomRepository.deleteAll();
        hotelRepository.deleteAll();
    }

    @Test
    public void shouldReturnAllRoomsWhenNoneBooked() {
        // given
        Hotel hotel = hotel(city);
        createRoom("11", 10, hotel);
        createRoom("12", 10, hotel);
        noBookings();

        // when
        List<AvailableRoomDto> availableRooms = roomService.findAvailableRooms(city, dateFrom, dateTo, BigDecimal.ZERO,
                BigDecimal.TEN);

        // then
        assertThat(availableRooms, hasSize(2));
    }

    @Test
    public void shouldReturnOnlyRoomFromSpecifiedCity() {
        // given
        createRoom("11", 10, hotel(city));
        createRoom("12", 10, hotel("other-than-" + city));
        noBookings();

        // when
        List<AvailableRoomDto> availableRooms = roomService.findAvailableRooms(city, dateFrom, dateTo, BigDecimal.ZERO,
                BigDecimal.TEN);

        // then
        assertThat(availableRooms, hasSize(1));
        assertThat(availableRooms.get(0).getRoomNumber(), is("11"));
    }

    @Test
    public void shouldReturnOnlyRoomsWithinPriceRange() {
        // given
        Hotel hotel = hotel(city);
        createRoom("11", 48, hotel);
        createRoom("12", 102, hotel);
        noBookings();

        // when
        List<AvailableRoomDto> availableRooms = roomService.findAvailableRooms(city, dateFrom, dateTo,
                BigDecimal.valueOf(50), BigDecimal.valueOf(102));

        // then
        assertThat(availableRooms, hasSize(1));
        assertThat(availableRooms.get(0).getRoomNumber(), is("12"));
    }

    @Test
    public void shouldReturnOnlyAvailableRoomsWithinPriceRange() {
        // given
        Hotel hotel = hotel(city);
        createRoom("11", 48, hotel);
        Room room2 = createRoom("12", 102, hotel);
        book(createCustomer(), room2, dateFrom.minusDays(1), dateTo.minusDays(1));

        // when
        List<AvailableRoomDto> availableRooms = roomService.findAvailableRooms(city, dateFrom, dateTo,
                BigDecimal.valueOf(50), BigDecimal.valueOf(102));

        // then
        assertThat(availableRooms, empty());
    }

    @Test
    public void shouldReturnRoomsWithinPriceRangeWhenReservationsDoesNotOverlap() {
        // given
        Hotel hotel = hotel(city);
        createRoom("11", 48, hotel);
        Room room2 = createRoom("12", 102, hotel);
        book(createCustomer(), room2, dateFrom.minusDays(10), dateTo.minusDays(10));

        // when
        List<AvailableRoomDto> availableRooms = roomService.findAvailableRooms(city, dateFrom, dateTo,
                BigDecimal.valueOf(50), BigDecimal.valueOf(102));

        // then
        assertThat(availableRooms, hasSize(1));
        assertThat(availableRooms.get(0).getRoomNumber(), is("12"));
    }

    private void noBookings() {
        // noBookings
    }

    private void book(Customer customer, Room room, LocalDate dateFrom, LocalDate dateTo) {
        reservationService.makeReservation(room.getId(), customer.getId(), dateFrom, dateTo);
    }

    private Customer createCustomer() {
        Customer customer = new Customer();
        customer.setEmail(email);
        return customerRepository.save(customer);
    }

    private Hotel hotel(String city) {
        Hotel hotel = new Hotel();
        hotel.setName("A Hotel in " + city);
        hotel.setCity(city);
        return hotelRepository.save(hotel);
    }

    private Room createRoom(String number, int price, Hotel hotel) {
        Room room = new Room();
        room.setNumber(number);
        room.setPrice(BigDecimal.valueOf(price));
        room.setHotel(hotel);
        return roomRepository.save(room);
    }
}