package pl.swapps.hreserv.domain.reservation;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.util.ReflectionUtils;
import pl.swapps.hreserv.domain.customer.Customer;
import pl.swapps.hreserv.domain.customer.CustomerService;
import pl.swapps.hreserv.domain.hotel.Hotel;
import pl.swapps.hreserv.domain.room.Room;
import pl.swapps.hreserv.domain.room.RoomService;
import pl.swapps.hreserv.exception.CustomerAlreadyReservedException;
import pl.swapps.hreserv.exception.NoSuchCustomerException;
import pl.swapps.hreserv.exception.NoSuchRoomException;
import pl.swapps.hreserv.exception.RoomAlreadyReservedException;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ReservationServiceTest {

    private final static Long hotelId = 26L;
    private final static String hotelName = "LeHotel";
    private final static String hotelCity = "Warszawa";
    private final static Long roomId = 123L;
    private final static String roomNumber = "13A";
    private final static BigDecimal oneDayPrice = BigDecimal.TEN;
    private final static Long customerId = 154L;
    private static final String customerEmail = "pjakubow@gmail.com";
    private final static Long reservationId = 4365L;
    private final static LocalDate dateFrom = LocalDate.parse("2018-01-03");
    private final static LocalDate dateTo = LocalDate.parse("2018-01-09");

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private RoomService roomService = Mockito.mock(RoomService.class);
    private CustomerService customerService = Mockito.mock(CustomerService.class);
    private ReservationRepository reservationRepository = Mockito.mock(ReservationRepository.class);
    private ReservationService reservationService = new ReservationService(customerService, roomService, reservationRepository);


    @Test
    public void shouldCreateReservation() {
        // given
        existingRoom(createRoom(roomId, roomNumber, oneDayPrice, createHotel(hotelId, hotelName, hotelCity)));
        existingCustomer(createCustomer(customerId, customerEmail));
        when(reservationRepository.save(any(Reservation.class))).thenAnswer(i -> addId((Reservation) i.getArguments()[0], reservationId));

        // when
        ReservationDto reservation = reservationService.makeReservation(roomId, customerId, dateFrom, dateTo);

        //then
        assertThat(reservation.getId(), notNullValue());
        assertThat(reservation.getRoom().getHotel().getName(), is(hotelName));
        assertThat(reservation.getRoom().getHotel().getCity(), is(hotelCity));
        assertThat(reservation.getCustomer().getId(), is(customerId));
        assertThat(reservation.getCustomer().getEmail(), is(customerEmail));
        assertThat(reservation.getDateFrom(), is(dateFrom));
        assertThat(reservation.getDateTo(), is(dateTo));
        assertThat(reservation.getDailyRoomPrice(), is(oneDayPrice));
        assertThat(reservation.getTotalCost(), is(oneDayPrice.multiply(BigDecimal.valueOf(6))));
    }

    @Test
    public void shouldThrowWhenRoomNotFound() {
        // given
        nonexistentRoom();

        // when, then
        exception.expect(NoSuchRoomException.class);
        reservationService.makeReservation(roomId, customerId, dateFrom, dateTo);
    }

    @Test
    public void shouldThrowWhenCustomerNotFound() {
        // given
        existingRoom(createRoom(roomId, roomNumber, oneDayPrice, createHotel(hotelId, hotelName, hotelCity)));
        nonexistentCustomer();

        // when, then
        exception.expect(NoSuchCustomerException.class);
        reservationService.makeReservation(roomId, customerId, dateFrom, dateTo);
    }

    @Test
    public void shouldThrowWhenRoomAlreadyReservedForOverlappingPeriod() {
        // given
        existingRoom(createRoom(roomId, roomNumber, oneDayPrice, createHotel(hotelId, hotelName, hotelCity)));
        existingCustomer(createCustomer(customerId, customerEmail));
        when(reservationRepository.overlappingRoomReservationExists(roomId, dateFrom, dateTo)).thenReturn(true);

        // when, then
        exception.expect(RoomAlreadyReservedException.class);
        reservationService.makeReservation(roomId, customerId, dateFrom, dateTo);
    }

    @Test
    public void shouldThrowWhenCustomerAlreadyReservedOtherRoomForOverlappingPeriod() {
        // given
        existingRoom(createRoom(roomId, roomNumber, oneDayPrice, createHotel(hotelId, hotelName, hotelCity)));
        existingCustomer(createCustomer(customerId, customerEmail));
        when(reservationRepository.overlappingCustomerReservationExists(customerId, dateFrom, dateTo)).thenReturn(true);

        // when, then
        exception.expect(CustomerAlreadyReservedException.class);
        reservationService.makeReservation(roomId, customerId, dateFrom, dateTo);
    }

    private void existingCustomer(Customer customer) {
        when(customerService.findById(customerId)).thenReturn(Optional.of(customer));
    }

    private void nonexistentCustomer() {
        when(customerService.findById(customerId)).thenReturn(Optional.empty());
    }

    private void existingRoom(Room room) {
        when(roomService.findById(roomId)).thenReturn(Optional.of(room));
    }

    private void nonexistentRoom() {
        when(roomService.findById(roomId)).thenReturn(Optional.empty());
    }

    private static Reservation addId(Reservation reservation, Long id) {
        setIdThroughReflection(Reservation.class, reservation, id);
        return reservation;
    }

    private static Room createRoom(Long id, String number, BigDecimal price, Hotel hotel) {
        Room room = new Room();
        setIdThroughReflection(Room.class, room, id);
        room.setNumber(number);
        room.setPrice(price);
        room.setHotel(hotel);
        return room;
    }

    private static Hotel createHotel(Long id, String name, String city) {
        Hotel hotel = new Hotel();
        setIdThroughReflection(Hotel.class, hotel, id);
        hotel.setName(name);
        hotel.setCity(city);
        return hotel;
    }

    private static Customer createCustomer(Long id, String email) {
        Customer customer = new Customer();
        setIdThroughReflection(Customer.class, customer, id);
        customer.setEmail(email);
        return customer;
    }

    private static void setIdThroughReflection(Class<?> clazz, Object entity, Long id) {
        Field idField = ReflectionUtils.findField(clazz, "id");
        idField.setAccessible(true);
        ReflectionUtils.setField(idField, entity, id);
        idField.setAccessible(false);
    }
}