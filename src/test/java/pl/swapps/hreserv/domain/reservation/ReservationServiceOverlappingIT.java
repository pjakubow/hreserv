package pl.swapps.hreserv.domain.reservation;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import pl.swapps.hreserv.domain.customer.CustomerRepository;
import pl.swapps.hreserv.domain.customer.CustomerService;
import pl.swapps.hreserv.domain.customer.CustomerDto;
import pl.swapps.hreserv.exception.CustomerAlreadyReservedException;
import pl.swapps.hreserv.exception.RoomAlreadyReservedException;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class ReservationServiceOverlappingIT {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private ReservationService reservationService;

    private static final Long room1Id = 12L;
    private static final Long room2Id = 17L;

    private Long customer1Id;
    private Long customer2Id;
    private Long reservationId;

    @Before
    public void setup() {
        CustomerDto customer1 = customerService.registerCustomer("pjakubow+1@gmail.com");
        CustomerDto customer2 = customerService.registerCustomer("pjakubow+2@gmail.com");
        customer1Id = customer1.getId();
        customer2Id = customer2.getId();
        ReservationDto reservationDto = reservationService.makeReservation(room1Id, customer1.getId(),
                LocalDate.parse("2018-09-01"), LocalDate.parse("2018-09-03"));
        reservationId = reservationDto.getId();
    }

    @After
    public void tearDown() {
        reservationRepository.deleteAll();
        customerRepository.deleteAll();
    }

    @Test
    public void shouldThrowWhenMakingReservationForTheSamePeriodForTheSameRoom() {
        assertOverlapsForTheSameRoom("2018-09-01", "2018-09-03");
    }

    @Test
    public void shouldThrowWhenMakingReservationForTheOverlappingPeriodForTheSameRoom() {
        assertOverlapsForTheSameRoom("2018-08-30", "2018-09-01");
    }

    @Test
    public void shouldThrowWhenMakingReservationForTheOverlappingPeriodForTheSameRoom_2() {
        assertOverlapsForTheSameRoom("2018-08-31", "2018-09-02");
    }

    @Test
    public void shouldThrowWhenMakingReservationForTheOverlappingPeriodForTheSameRoom_3() {
        assertOverlapsForTheSameRoom("2018-09-02", "2018-09-03");
    }

    @Test
    public void shouldThrowWhenMakingReservationForTheOverlappingPeriodForTheSameRoom_4() {
        assertOverlapsForTheSameRoom("2018-09-02", "2018-09-04");

    }

    @Test
    public void shouldThrowWhenMakingReservationForTheOverlappingPeriodForTheSameRoom_5() {
        assertOverlapsForTheSameRoom("2018-09-03", "2018-09-04");
    }

    @Test
    public void shouldThrowWhenMakingReservationForTheSamePeriodForTheSameCustomer() {
        assertOverlapsForTheSameCustomer("2018-09-01", "2018-09-03");
    }

    @Test
    public void shouldThrowWhenMakingReservationForTheOverlappingPeriodForTheSameCustomer() {
        assertOverlapsForTheSameCustomer("2018-08-30", "2018-09-01");
    }

    @Test
    public void shouldThrowWhenMakingReservationForTheOverlappingPeriodForTheSameCustomer_2() {
        assertOverlapsForTheSameCustomer("2018-08-31", "2018-09-02");
    }

    @Test
    public void shouldThrowWhenMakingReservationForTheOverlappingPeriodForTheSameCustomer_3() {
        assertOverlapsForTheSameCustomer("2018-09-02", "2018-09-03");
    }

    @Test
    public void shouldThrowWhenMakingReservationForTheOverlappingPeriodForTheSameCustomer_4() {
        assertOverlapsForTheSameCustomer("2018-09-02", "2018-09-04");

    }

    @Test
    public void shouldThrowWhenMakingReservationForTheOverlappingPeriodForTheSameCustomer_5() {
        assertOverlapsForTheSameCustomer("2018-09-03", "2018-09-04");
    }

    private void assertOverlapsForTheSameRoom(String from, String to) {
        // given, when, then
        exception.expect(RoomAlreadyReservedException.class);
        reservationService.makeReservation(room1Id, customer2Id, LocalDate.parse(from), LocalDate.parse(to));
        // and
        assertThat(reservationRepository.count(), is(1));
    }

    private void assertOverlapsForTheSameCustomer(String from, String to) {
        // given, when, then
        exception.expect(CustomerAlreadyReservedException.class);
        reservationService.makeReservation(room2Id, customer1Id, LocalDate.parse(from), LocalDate.parse(to));
        // and
        assertThat(reservationRepository.count(), is(1));
    }
}