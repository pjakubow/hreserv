package pl.swapps.hreserv;

import io.restassured.http.ContentType;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static io.restassured.RestAssured.with;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HreservAcceptance {

    private static final String customerEmail = "pjakubow+" + UUID.randomUUID() + "@gmail.com";
    private static final String dateFrom = "2118-09-01";
    private static final String dateTo = "2118-09-08";
    private static Long customerId;
    private static Long roomId;
    private static Long reservationId;

    private String baseUri = System.getProperty("hreserv.url", "http://localhost:8080");

    private RequestSpecification request;

    @Before
    public void setUp() {
        request = with().baseUri(baseUri);
    }

    @Test
    public void _010_shouldRegisterNewCustomer() {

        Map<String, Object> json = new HashMap<>();
        json.put("email", customerEmail);

        String customerUri = request
                .given()
                    .contentType(ContentType.JSON)
                    .body(json)
                .when()
                    .post("/customers")
                .then()
                    .statusCode(201)
                    .header("Location", startsWith("/customers/"))
                .extract()
                    .header("Location");

        customerId = Long.parseLong(customerUri.substring(customerUri.lastIndexOf('/') + 1));
    }

    @Test
    public void _020_shouldNotRegisterSameCustomerAgain() {

        Map<String, Object> json = new HashMap<>();
        json.put("email", customerEmail);

        request
                .given()
                    .contentType(ContentType.JSON)
                    .body(json)
                .when()
                    .post("/customers")
                .then()
                    .statusCode(400)
                    .body(
                            "code", equalTo(3002),
                            "message", containsString(customerEmail)
                    );
    }

    @Test
    public void _030_shouldFindHotelsFromRomaIgnoringCase() {
        ExtractableResponse<Response> extract = request
                .when()
                    .get("/rooms?city=rOmA&dateFrom=" + dateFrom + "&dateTo=" + dateTo + "&priceFrom=90")
                .then()
                    .statusCode(200)
                    .body(
                            "$", hasSize(4),
                            "$", hasItem(hasEntry("hotelCity", "Roma")),
                            "$", hasItem(hasEntry("hotelName", "Midas")),
                            "$", hasItem(hasKey("roomNumber")),
                            "$", hasItem(hasKey("dailyRoomPrice")),
                            "$", hasItem(hasKey("totalCost")),
                            "$", hasItem(hasKey("hotelId")),
                            "$", hasItem(hasKey("id"))
                    )
                .extract();

        roomId = ((Integer) ((List<Map<String, Object>>) extract.path("")).get(0).get("id")).longValue();
    }

    @Test
    public void _040_shouldReturnSimpleValidationErrorsWhenRequiredParamsAreMissing() {
        request
                .when()
                    .get("/rooms")
                .then()
                    .statusCode(400)
                    .body(
                            "code", equalTo(1000),
                            "message", equalTo("validation_error"),
                            "fieldErrors", hasItem(hasEntry("field", "city")),
                            "fieldErrors", hasItem(hasEntry("field", "dateFrom")),
                            "fieldErrors", hasItem(hasEntry("field", "dateTo")),
                            "fieldErrors", hasItem(hasEntry("message", "must not be null")),
                            "fieldErrors", hasItem(hasEntry("rejectedValue", null))
                    );
    }

    @Test
    public void _050_shouldMakeReservation() {

        Map<String, Object> json = new HashMap<>();
        json.put("customerId", customerId);
        json.put("dateFrom", dateFrom);
        json.put("dateTo", dateTo);

        String reservationUri = request
                .given()
                    .contentType(ContentType.JSON)
                    .body(json)
                .when()
                    .post("/rooms/" + roomId + "/reservations")
                .then()
                    .statusCode(201)
                    .header("Location", startsWith("/reservations/"))
                .extract()
                    .header("location");

        reservationId = Long.parseLong(reservationUri.substring(reservationUri.lastIndexOf('/') + 1));
    }

    @Test
    public void _060_shouldNotFindReservedRoomInRoma() {
        request
                .when()
                    .get("/rooms?city=rOmA&dateFrom=" + dateFrom + "&dateTo=" + dateTo + "&priceFrom=90")
                .then()
                    .statusCode(200)
                    .body(
                            "$", hasSize(3),
                            "$", not(hasItem(hasEntry("id", roomId)))
                    );
    }

    @Test
    public void _070_shouldNotAllowToDeleteSomeoneElsesReservation() {
        request
                .when()
                    .delete("/customers/" + (customerId + 10) + "/reservations/" + reservationId)
                .then()
                    .statusCode(400)
                    .body(
                            "code", equalTo(6001),
                            "message", containsString("Cannot remove")
                    );
    }

    @Test
    public void _080_shouldDeleteReservation() {
        request
                .when()
                    .delete("/customers/" + customerId + "/reservations/" + reservationId)
                .then()
                    .statusCode(204);
    }
}
