package pl.swapps.hreserv.validation;

import org.hibernate.validator.internal.metadata.descriptor.ConstraintDescriptorImpl;
import org.junit.Before;
import org.junit.Test;
import pl.swapps.hreserv.boundary.rest.protocol.request.ReservationRequest;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDate;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DatesInOrderValidationTest {

    private final LocalDate now = LocalDate.now();
    private Validator validator;

    @Before
    public void setup() {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        validator = vf.getValidator();
    }

    @Test
    public void shouldFoundViolationWhenDateFromIsAfterDateTo() {
        // given
        ReservationRequest request = createReservationRequest(now.plusDays(20), now.plusDays(10));

        // when
        Set<ConstraintViolation<ReservationRequest>> violations = validator.validate(request);

        // then
        assertTrue(hasViolation(violations, DatesInOrder.class, "dateTo"));
    }

    @Test
    public void shouldFoundViolationWhenDateFromIsEqualToDateTo() {
        // given
        ReservationRequest request = createReservationRequest(now.plusDays(10), now.plusDays(10));

        // when
        Set<ConstraintViolation<ReservationRequest>> violations = validator.validate(request);

        // then
        assertTrue(hasViolation(violations, DatesInOrder.class, "dateTo"));
    }

    @Test
    public void shouldNotFoundViolationsWhenDateFromIsBeforeDateTo() {
        // given
        ReservationRequest request = createReservationRequest(now.plusDays(10), now.plusDays(20));

        // when
        Set<ConstraintViolation<ReservationRequest>> violations = validator.validate(request);

        // then
        assertFalse(hasViolation(violations, DatesInOrder.class, "dateTo"));
    }

    @Test
    public void shouldFoundViolationWhenDateFromAndDateToAreNull() {
        // given
        ReservationRequest request = createReservationRequest(null, null);

        // when
        Set<ConstraintViolation<ReservationRequest>> violations = validator.validate(request);

        // then
        assertTrue(hasViolation(violations, DatesInOrder.class, "dateTo"));
    }

    private static ReservationRequest createReservationRequest(LocalDate dateFrom, LocalDate dateTo) {
        ReservationRequest request = new ReservationRequest();
        request.setCustomerId(1L);
        request.setDateFrom(dateFrom);
        request.setDateTo(dateTo);
        return request;
    }

    private static <T, C> boolean hasViolation(Set<ConstraintViolation<T>> constraintViolations, Class<C> annotationClass, String
            propertyPath) {
        return constraintViolations
                .stream()
                .anyMatch(it -> it.getPropertyPath().toString().equals(propertyPath) && isOfType(it, annotationClass));
    }

    private static boolean isOfType(ConstraintViolation<?> violation, Class<?> clazz) {
        return ((ConstraintDescriptorImpl) violation.getConstraintDescriptor()).getAnnotationType().isAssignableFrom(clazz);
    }
}