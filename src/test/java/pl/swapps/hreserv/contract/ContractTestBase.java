package pl.swapps.hreserv.contract;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.UriComponentsBuilder;
import pl.swapps.hreserv.domain.customer.Customer;
import pl.swapps.hreserv.domain.customer.CustomerRepository;
import pl.swapps.hreserv.domain.reservation.Reservation;
import pl.swapps.hreserv.domain.reservation.ReservationRepository;
import pl.swapps.hreserv.domain.room.Room;
import pl.swapps.hreserv.domain.room.RoomRepository;

import java.math.BigDecimal;
import java.time.LocalDate;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public abstract class ContractTestBase {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    private Customer customer;
    private Reservation reservation;

    @Before
    public void setup() {
        Room room = findRoom();
        customer = createTestCustomer();
        reservation = createTestReservation(customer, room);
        RestAssuredMockMvc.webAppContextSetup(webApplicationContext);
    }

    @After
    public void tearDown() {
        reservationRepository.delete(reservation);
    }

    String getTestCustomerReservationsUri() {
        return UriComponentsBuilder.fromUriString("/customers")
                .pathSegment(String.valueOf(customer.getId()), "reservations")
                .toUriString();
    }

    String getTestReservationsDeleteUri() {
        return UriComponentsBuilder.fromUriString("/customers")
                .pathSegment(String.valueOf(customer.getId()), "reservations", String.valueOf(reservation.getId()))
                .toUriString();
    }

    private Reservation createTestReservation(Customer customer, Room room) {

        Reservation reservation = new Reservation();
        reservation.setCustomer(customer);
        reservation.setRoom(room);
        reservation.setDailyRoomPrice(room.getPrice());
        reservation.setTotalCost(room.getPrice().multiply(BigDecimal.valueOf(9)));
        reservation.setDateFrom(LocalDate.parse("2018-09-01"));
        reservation.setDateTo(LocalDate.parse("2018-09-10"));

        return reservationRepository.save(reservation);
    }

    private Room findRoom() {
        return roomRepository.findAll().get(0);
    }

    private Customer createTestCustomer() {
        Customer customer = new Customer();
        customer.setEmail("pjakubow@gmail.com");
        return customerRepository.save(customer);
    }
}
