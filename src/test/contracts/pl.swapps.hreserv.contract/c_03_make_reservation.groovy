package pl.swapps.hreserv.contract

import org.springframework.cloud.contract.spec.Contract

Contract.make {

    priority 3

    description "should create a reservation for customer when room is available"

    request {
        url "/rooms/1/reservations"
        method POST()
        body (
                customerId: 1,
                dateFrom: "2118-03-03",
                dateTo: "2118-03-06"
        )
        headers {
            contentType applicationJson()
        }
    }

    response {
        status 201
        headers {
            header 'Location' : $(regex('^/reservations/\\\\d+$'))
        }
    }
}