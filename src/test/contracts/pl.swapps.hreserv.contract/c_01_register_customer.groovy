package pl.swapps.hreserv.contract

import org.springframework.cloud.contract.spec.Contract

Contract.make {

    priority 1

    description "should register customer and return his/her id in location header"

    request {
        url "/customers"
        method POST()
        body (
                email: "piotrek@swapps.pl"
        )
        headers {
            contentType applicationJson()
        }
    }

    response {
        status 201
        headers {
            header 'Location' : $(producer(regex('^/customers/\\\\d+$')))
        }
    }
}