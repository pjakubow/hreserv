package pl.swapps.hreserv.contract

import org.springframework.cloud.contract.spec.Contract

Contract.make {

    priority 5

    description "should remove reservation"

    request {
        url $(c("/customers/1/reservations/1"), p(execute("getTestReservationsDeleteUri()")))
        method DELETE()
    }

    response {
        status 204
    }
}