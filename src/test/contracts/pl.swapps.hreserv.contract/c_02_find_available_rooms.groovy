package pl.swapps.hreserv.contract

import org.springframework.cloud.contract.spec.Contract

Contract.make {

    priority 2

    description "should find customer's reservations"

    request {
        url "/rooms?city=Warszawa&dateFrom=2118-01-01&dateTo=2118-01-08&priceFrom=0&priceTo=100"
        method GET()
    }

    response {
        status 200
        body(
                [[
                        id             : $(p(positiveInt())),
                        hotelId        : $(p(positiveInt())),
                        hotelName      : $(p(nonBlank())),
                        hotelCity      : $(p(nonBlank())),
                        roomNumber     : $(p(alphaNumeric())),
                        dailyRoomPrice : $(p(anyNumber())),
                        totalCost      : $(p(anyNumber()))
                ]]
        )
    }
}