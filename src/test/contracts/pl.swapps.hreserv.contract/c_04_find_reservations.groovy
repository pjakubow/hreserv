package pl.swapps.hreserv.contract

import org.springframework.cloud.contract.spec.Contract

Contract.make {

    priority 4

    description "should find customer's reservations"

    request {
        url $(c('/customers/1/reservations'), p(execute('getTestCustomerReservationsUri()')))
        method GET()
    }

    response {
        status 200
        body(
                [[
                        id             : $(p(positiveInt())),
                        hotelId        : $(p(positiveInt())),
                        hotelName      : $(p(nonBlank())),
                        hotelCity      : $(p(nonBlank())),
                        roomId         : $(p(positiveInt())),
                        roomNumber     : $(p(alphaNumeric())),
                        dateFrom       : $(p(isoDate())),
                        dateTo         : $(p(isoDate())),
                        dailyRoomPrice : $(p(anyNumber())),
                        totalCost      : $(p(anyNumber()))
                ]]
        )
    }
}