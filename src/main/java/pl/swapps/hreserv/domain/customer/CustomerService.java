package pl.swapps.hreserv.domain.customer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.swapps.hreserv.exception.CustomerEmailAlreadyExistsException;

import java.util.Optional;

@Slf4j
@Service
@Transactional
public class CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Optional<Customer> findById(Long customerId) {
        return customerRepository.findById(customerId);
    }

    public CustomerDto registerCustomer(String email) {
        String normalizedEmail = email.toLowerCase();
        log.info("Registering customer with email: {} (normalized: {})", email, normalizedEmail);
        if (customerRepository.existsByEmailIgnoreCase(normalizedEmail)) {
            throw new CustomerEmailAlreadyExistsException(normalizedEmail);
        }
        Customer customer = new Customer();
        customer.setEmail(normalizedEmail);
        return CustomerMapper.map(customerRepository.save(customer));
    }
}
