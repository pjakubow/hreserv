package pl.swapps.hreserv.domain.customer;

class CustomerMapper {

    static CustomerDto map(Customer customer) {
        return new CustomerDto(customer.getId(), customer.getEmail());
    }
}
