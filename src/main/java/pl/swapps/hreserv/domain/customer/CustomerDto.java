package pl.swapps.hreserv.domain.customer;

import lombok.Value;

@Value
public class CustomerDto {

    private Long id;
    private String email;
}
