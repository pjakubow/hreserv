package pl.swapps.hreserv.domain.customer;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import pl.swapps.hreserv.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;

@Getter
@Setter
@Entity
@Table(name = "customers")
public class Customer extends BaseEntity {

    private static final String SEQUENCE_NAME = "customers_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    @GenericGenerator(name = SEQUENCE_NAME,
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = SEQUENCE_NAME),
                    @Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            })
    @Setter(AccessLevel.NONE)
    private Long id;

    @Email
    @Column(unique = true)
    private String email;
}
