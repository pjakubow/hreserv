package pl.swapps.hreserv.domain.reservation;

import pl.swapps.hreserv.domain.hotel.Hotel;
import pl.swapps.hreserv.domain.room.Room;

class CustomerReservationMapper {

    static CustomerReservationDto map(Reservation reservation) {
        Room room = reservation.getRoom();
        Hotel hotel = room.getHotel();
        return CustomerReservationDto.builder()
                .id(reservation.getId())
                .hotelId(hotel.getId())
                .hotelName(hotel.getName())
                .hotelCity(hotel.getCity())
                .roomId(room.getId())
                .roomNumber(room.getNumber())
                .dailyRoomPrice(reservation.getDailyRoomPrice())
                .totalCost(reservation.getTotalCost())
                .dateFrom(reservation.getDateFrom())
                .dateTo(reservation.getDateTo())
                .build();
    }
}
