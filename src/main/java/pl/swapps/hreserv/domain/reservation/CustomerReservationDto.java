package pl.swapps.hreserv.domain.reservation;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;


@Builder
@Getter
@ToString
@EqualsAndHashCode
public class CustomerReservationDto {

    private Long id;
    private Long hotelId;
    private String hotelName;
    private String hotelCity;
    private Long roomId;
    private String roomNumber;
    private BigDecimal dailyRoomPrice;
    private BigDecimal totalCost;
    private LocalDate dateFrom;
    private LocalDate dateTo;
}
