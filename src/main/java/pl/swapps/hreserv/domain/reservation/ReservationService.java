package pl.swapps.hreserv.domain.reservation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.swapps.hreserv.domain.customer.Customer;
import pl.swapps.hreserv.domain.customer.CustomerService;
import pl.swapps.hreserv.domain.room.Room;
import pl.swapps.hreserv.domain.room.RoomService;
import pl.swapps.hreserv.exception.CustomerAlreadyReservedException;
import pl.swapps.hreserv.exception.NoSuchCustomerException;
import pl.swapps.hreserv.exception.NoSuchReservationException;
import pl.swapps.hreserv.exception.NoSuchRoomException;
import pl.swapps.hreserv.exception.RoomAlreadyReservedException;
import pl.swapps.hreserv.exception.UnauthorizedOperationException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Slf4j
@Service
@Transactional
public class ReservationService {

    private final CustomerService customerService;
    private final RoomService roomService;
    private final ReservationRepository reservationRepository;

    public ReservationService(CustomerService customerService, RoomService roomService, ReservationRepository reservationRepository) {
        this.customerService = customerService;
        this.roomService = roomService;
        this.reservationRepository = reservationRepository;
    }

    public ReservationDto makeReservation(Long roomId, Long customerId, LocalDate dateFrom, LocalDate dateTo) {

        log.info("Requested reservation for roomId: {} on {} -> {} by customerId: {}", roomId, dateFrom, dateTo, customerId);

        final Room room = roomService.findById(roomId).orElseThrow(() -> new NoSuchRoomException(roomId));
        final Customer customer = customerService.findById(customerId).orElseThrow(() -> new NoSuchCustomerException(customerId));

        if (reservationRepository.overlappingRoomReservationExists(roomId, dateFrom, dateTo)) {
            throw new RoomAlreadyReservedException(roomId, dateFrom, dateTo);
        }

        if (reservationRepository.overlappingCustomerReservationExists(customerId, dateFrom, dateTo)) {
            throw new CustomerAlreadyReservedException(customerId, dateFrom, dateTo);
        }

        Reservation reservation = new Reservation();
        reservation.setRoom(room);
        reservation.setCustomer(customer);
        reservation.setDateFrom(dateFrom);
        reservation.setDateTo(dateTo);
        reservation.setDailyRoomPrice(room.getPrice());
        reservation.setTotalCost(room.getPrice().multiply(BigDecimal.valueOf(DAYS.between(dateFrom, dateTo))));

        Reservation savedReservation = reservationRepository.save(reservation);
        log.info("Successfully created reservationId: {} for roomId: {} on {} -> {} by customerId: {}", savedReservation.getId(), roomId,
                dateFrom, dateTo, customerId);
        return ReservationMapper.map(savedReservation);
    }

    public List<CustomerReservationDto> findCustomerReservations(Long customerId) {

        return reservationRepository.findAllByCustomerId(customerId).stream()
                .map(CustomerReservationMapper::map)
                .collect(Collectors.toList());
    }

    public void removeReservation(Long customerId, Long reservationId) {

        log.info("Requested to remove reservationId: {} by customerId: {}", reservationId, customerId);
        Reservation reservation = reservationRepository.findById(reservationId)
                .orElseThrow(() -> new NoSuchReservationException(reservationId));

        if (!Objects.equals(customerId, reservation.getCustomer().getId())) {
            log.warn("Unauthorized attempt to remove reservationId: {} by customerId: {}", reservationId, customerId);
            throw new UnauthorizedOperationException("Cannot remove reservation");
        }
        reservationRepository.delete(reservation);
    }
}
