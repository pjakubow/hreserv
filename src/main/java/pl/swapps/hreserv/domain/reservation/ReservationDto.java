package pl.swapps.hreserv.domain.reservation;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDate;

@Builder
@Getter
@ToString
@EqualsAndHashCode
public class ReservationDto {

    private Long id;
    private RoomDto room;
    private CustomerDto customer;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private BigDecimal dailyRoomPrice;
    private BigDecimal totalCost;

    @Value
    public static class RoomDto {

        private Long id;
        private String number;
        private HotelDto hotel;
    }

    @Value
    public static class HotelDto {

        private Long id;
        private String name;
        private String city;
    }

    @Value
    public static class CustomerDto {

        private Long id;
        private String email;
    }
}
