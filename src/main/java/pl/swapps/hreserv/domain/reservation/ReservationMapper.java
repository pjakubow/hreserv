package pl.swapps.hreserv.domain.reservation;

import pl.swapps.hreserv.domain.customer.Customer;
import pl.swapps.hreserv.domain.hotel.Hotel;
import pl.swapps.hreserv.domain.room.Room;

class ReservationMapper {

    static ReservationDto map(Reservation reservation) {
        Room room = reservation.getRoom();
        return ReservationDto.builder()
                .id(reservation.getId())
                .room(map(room))
                .customer(map(reservation.getCustomer()))
                .dateFrom(reservation.getDateFrom())
                .dateTo(reservation.getDateTo())
                .dailyRoomPrice(reservation.getDailyRoomPrice())
                .totalCost(reservation.getTotalCost())
                .build();
    }

    private static ReservationDto.RoomDto map(Room room) {
        Hotel hotel = room.getHotel();
        ReservationDto.HotelDto hotelDto = new ReservationDto.HotelDto(hotel.getId(), hotel.getName(), hotel.getCity());
        return new ReservationDto.RoomDto(room.getId(), room.getNumber(), hotelDto);
    }

    private static ReservationDto.CustomerDto map(Customer customer) {
        return new ReservationDto.CustomerDto(customer.getId(), customer.getEmail());
    }
}
