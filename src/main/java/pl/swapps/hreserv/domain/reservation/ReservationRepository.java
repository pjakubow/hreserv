package pl.swapps.hreserv.domain.reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    List<Reservation> findAllByCustomerId(Long customerId);

    @Query("select count(rs) > 0 from Reservation rs where rs.room.id = :roomId and rs.dateTo >= :dateFrom and rs.dateFrom <= :dateTo")
    boolean overlappingRoomReservationExists(@Param("roomId") Long roomId, @Param("dateFrom") LocalDate dateFrom,
            @Param("dateTo") LocalDate dateTo);

    @Query("select count(rs) > 0 from Reservation rs where rs.customer.id = :customerId and rs.dateTo >= :dateFrom"
            + " and rs.dateFrom <= :dateTo")
    boolean overlappingCustomerReservationExists(@Param("customerId") Long customerId, @Param("dateFrom") LocalDate dateFrom,
            @Param("dateTo") LocalDate dateTo);
}
