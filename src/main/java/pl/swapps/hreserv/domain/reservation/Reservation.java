package pl.swapps.hreserv.domain.reservation;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import pl.swapps.hreserv.BaseEntity;
import pl.swapps.hreserv.domain.customer.Customer;
import pl.swapps.hreserv.domain.room.Room;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "reservations")
public class Reservation extends BaseEntity {

    private static final String SEQUENCE_NAME = "reservations_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    @GenericGenerator(name = SEQUENCE_NAME,
            strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {
                    @Parameter(name = SequenceStyleGenerator.SEQUENCE_PARAM, value = SEQUENCE_NAME),
                    @Parameter(name = SequenceStyleGenerator.INCREMENT_PARAM, value = "1")
            })
    @Setter(AccessLevel.NONE)

    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id", nullable = false, updatable = false)
    private Room room;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id", nullable = false, updatable = false)
    private Customer customer;

    private LocalDate dateFrom;

    private LocalDate dateTo;

    private BigDecimal dailyRoomPrice;

    private BigDecimal totalCost;
}
