package pl.swapps.hreserv.domain.room;

import pl.swapps.hreserv.domain.hotel.Hotel;

import java.math.BigDecimal;

class RoomMapper {

    static AvailableRoomDto mapToAvailableRoomDto(Room room, long daysBetween) {
        Hotel hotel = room.getHotel();
        return AvailableRoomDto.builder()
                .id(room.getId())
                .hotelId(hotel.getId())
                .hotelName(hotel.getName())
                .hotelCity(hotel.getCity())
                .roomNumber(room.getNumber())
                .dailyRoomPrice(room.getPrice())
                .totalCost(room.getPrice().multiply(BigDecimal.valueOf(daysBetween)))
                .build();
    }
}
