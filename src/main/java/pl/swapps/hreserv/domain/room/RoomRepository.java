package pl.swapps.hreserv.domain.room;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

    @Query("from Room r "
            + "where lower(r.hotel.city) like lower(:city) "
            + "and r.price >= :priceFrom "
            + "and r.price <= :priceTo "
            + "and r.id not in ( "
                + "select r1.id from Room r1, Reservation rs "
                    + "where rs.room.id = r1.id "
                    + "and lower(r1.hotel.city) like lower(:city) "
                    + "and r1.price >= :priceFrom "
                    + "and r1.price <= :priceTo "
                    + "and rs.dateFrom <= :dateTo "
                    + "and rs.dateTo >= :dateFrom"
            + ")")
    List<Room> findAvailable(@Param("city") String city, @Param("dateFrom") LocalDate dateFrom, @Param("dateTo") LocalDate dateTo,
            @Param("priceFrom") BigDecimal priceFrom, @Param("priceTo") BigDecimal priceTo);
}
