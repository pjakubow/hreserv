package pl.swapps.hreserv.domain.room;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
@Transactional(readOnly = true)
public class RoomService {

    private final RoomRepository roomRepository;

    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public Optional<Room> findById(Long roomId) {
        return roomRepository.findById(roomId);
    }

    public List<AvailableRoomDto> findAvailableRooms(String city, LocalDate dateFrom, LocalDate dateTo, BigDecimal priceFrom,
            BigDecimal priceTo) {
        long daysBetween = DAYS.between(dateFrom, dateTo);
        return roomRepository.findAvailable(city, dateFrom, dateTo, priceFrom, priceTo).stream()
                .map(room -> RoomMapper.mapToAvailableRoomDto(room, daysBetween))
                .collect(Collectors.toList());
    }
}
