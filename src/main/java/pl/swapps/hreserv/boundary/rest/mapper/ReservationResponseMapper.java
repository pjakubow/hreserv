package pl.swapps.hreserv.boundary.rest.mapper;

import pl.swapps.hreserv.boundary.rest.protocol.response.ReservationListItemResponse;
import pl.swapps.hreserv.domain.reservation.CustomerReservationDto;

public class ReservationResponseMapper {

    public static ReservationListItemResponse map(CustomerReservationDto dto) {
        return ReservationListItemResponse.builder()
                .id(dto.getId())
                .hotelId(dto.getHotelId())
                .hotelName(dto.getHotelName())
                .hotelCity(dto.getHotelCity())
                .roomId(dto.getRoomId())
                .roomNumber(dto.getRoomNumber())
                .dailyRoomPrice(dto.getDailyRoomPrice())
                .totalCost(dto.getTotalCost())
                .dateFrom(dto.getDateFrom())
                .dateTo(dto.getDateTo())
                .build();
    }
}
