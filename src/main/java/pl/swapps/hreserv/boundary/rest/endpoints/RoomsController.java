package pl.swapps.hreserv.boundary.rest.endpoints;


import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import pl.swapps.hreserv.boundary.rest.mapper.AvailableRoomResponseMapper;
import pl.swapps.hreserv.boundary.rest.protocol.response.AvailableRoomResponse;
import pl.swapps.hreserv.boundary.rest.protocol.request.AvailableRoomsSearchRequest;
import pl.swapps.hreserv.boundary.rest.protocol.request.ReservationRequest;
import pl.swapps.hreserv.domain.reservation.ReservationDto;
import pl.swapps.hreserv.domain.reservation.ReservationService;
import pl.swapps.hreserv.domain.room.AvailableRoomDto;
import pl.swapps.hreserv.domain.room.RoomService;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rooms")
public class RoomsController {

    private final RoomService roomService;
    private final ReservationService reservationService;

    public RoomsController(RoomService roomService, ReservationService reservationService) {
        this.roomService = roomService;
        this.reservationService = reservationService;
    }

    @GetMapping
    @ApiOperation(value = "Search for available rooms in a specified city ", produces = "application/json")
    public ResponseEntity<List<AvailableRoomResponse>> findAvailableRooms(@Valid AvailableRoomsSearchRequest request) {
        List<AvailableRoomDto> availableRooms = roomService.findAvailableRooms(request.getCity(), request.getDateFrom(),
                request.getDateTo(), request.getPriceFrom(), request.getPriceTo());
        return ResponseEntity.ok(availableRooms.stream().map(AvailableRoomResponseMapper::map).collect(Collectors.toList()));
    }

    @PostMapping("/{roomId}/reservations")
    @ApiOperation(value = "Make a reservation", consumes = "application/json")
    public ResponseEntity makeReservation(@PathVariable Long roomId, @Valid @RequestBody ReservationRequest reservationRequest) {
        ReservationDto reservation = reservationService.makeReservation(roomId, reservationRequest.getCustomerId(), reservationRequest
                .getDateFrom(), reservationRequest.getDateTo());
        return ResponseEntity.created(buildCreatedUri("/reservations", reservation)).build();
    }

    private URI buildCreatedUri(String uri, ReservationDto reservation) {
        return UriComponentsBuilder.fromUriString(uri)
                .pathSegment(String.valueOf(reservation.getId()))
                .build()
                .toUri();
    }
}
