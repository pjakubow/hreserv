package pl.swapps.hreserv.boundary.rest.errors;

import lombok.Value;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import pl.swapps.hreserv.exception.ErrorCodes;
import pl.swapps.hreserv.exception.HReservException;

import java.util.List;
import java.util.stream.Collectors;

class ErrorResponseCreator {

    static ErrorResponse create(HReservException ex) {
        return new ErrorResponse(ex.getCode(), ex.getMessage());
    }

    static ValidationErrorResponse create(Errors errors) {
        List<FieldError> fieldErrors = errors.getFieldErrors();
        List<ObjectError> globalErrors = errors.getGlobalErrors();
        return new ValidationErrorResponse(
                fieldErrors.stream().map(ErrorResponseCreator::mapFieldError).collect(Collectors.toList()),
                globalErrors.stream().map(ErrorResponseCreator::mapGlobalError).collect(Collectors.toList()));
    }

    private static FieldErrorResponse mapFieldError(FieldError fieldError) {
        return new FieldErrorResponse(
                fieldError.getField(),
                fieldError.getDefaultMessage(),
                fieldError.getRejectedValue()
        );
    }

    private static GlobalErrorResponse mapGlobalError(ObjectError globalError) {
        return new GlobalErrorResponse(
                globalError.getObjectName(),
                globalError.getDefaultMessage()
        );
    }

    @Value
    static class ErrorResponse {
        private int code;
        private String message;
    }

    @Value
    static class ValidationErrorResponse {
        private int code = ErrorCodes.VALIDATION_ERROR;
        private String message = "validation_error";
        private List<FieldErrorResponse> fieldErrors;
        private List<GlobalErrorResponse> globalErrors;
    }


    @Value
    private static class FieldErrorResponse {
        private String field;
        private String message;
        private Object rejectedValue;
    }

    @Value
    private static class GlobalErrorResponse {
        private String object;
        private String message;
    }
}
