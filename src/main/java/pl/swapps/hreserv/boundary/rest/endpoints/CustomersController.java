package pl.swapps.hreserv.boundary.rest.endpoints;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import pl.swapps.hreserv.domain.customer.CustomerService;
import pl.swapps.hreserv.domain.reservation.ReservationService;
import pl.swapps.hreserv.boundary.rest.mapper.ReservationResponseMapper;
import pl.swapps.hreserv.boundary.rest.protocol.request.RegisterCustomerRequest;
import pl.swapps.hreserv.boundary.rest.protocol.response.ReservationListItemResponse;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/customers")
public class CustomersController {

    private final CustomerService customerService;
    private final ReservationService reservationService;

    public CustomersController(CustomerService customerService, ReservationService reservationService) {
        this.customerService = customerService;
        this.reservationService = reservationService;
    }

    @PostMapping
    @ApiOperation("Register new customer")
    public ResponseEntity registerCustomer(@Valid @RequestBody RegisterCustomerRequest request) {
        Long customerId = customerService.registerCustomer(request.getEmail()).getId();
        return ResponseEntity.created(UriComponentsBuilder.fromUriString("/customers")
                .pathSegment(String.valueOf(customerId))
                .build()
                .toUri()
        ).build();
    }

    @GetMapping("/{customerId}/reservations")
    @ApiOperation("List customer reservations")
    public ResponseEntity<List<ReservationListItemResponse>> listCustomerReservations(@PathVariable Long customerId) {
        return ResponseEntity.ok(reservationService.findCustomerReservations(customerId).stream()
                .map(ReservationResponseMapper::map)
                .collect(Collectors.toList()));
    }

    @DeleteMapping("/{customerId}/reservations/{reservationId}")
    @ApiOperation("Remove a customer reservation")
    public ResponseEntity removeReservation(@PathVariable Long customerId, @PathVariable Long reservationId) {
        reservationService.removeReservation(customerId, reservationId);
        return ResponseEntity.noContent().build();
    }
}
