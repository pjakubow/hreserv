package pl.swapps.hreserv.boundary.rest.protocol.request;

import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class RegisterCustomerRequest {

    @Email
    @NotBlank
    @ApiParam(example = "user@server.pl", required = true)
    private String email;
}
