package pl.swapps.hreserv.boundary.rest.protocol.request;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import pl.swapps.hreserv.validation.DatesInOrder;

import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@DatesInOrder(before = "dateFrom", after = "dateTo")
public class AvailableRoomsSearchRequest {

    @NotBlank
    @ApiParam(example = "Warszawa", required = true, allowableValues = "Warszawa, Kraków, Roma")
    private String city;

    @NotNull
    @FutureOrPresent
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @ApiParam(example = "2019-03-15", required = true, format = "YYYY-MM-DD")
    private LocalDate dateFrom;

    @NotNull
    @Future
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @ApiParam(example = "2019-03-25", required = true, format = "YYYY-MM-DD")
    private LocalDate dateTo;

    @Min(0)
    @ApiParam(example = "100", defaultValue = "0")
    private BigDecimal priceFrom = BigDecimal.ZERO;

    @ApiParam(example = "190", defaultValue = "" + Integer.MAX_VALUE)
    private BigDecimal priceTo = BigDecimal.valueOf(Integer.MAX_VALUE);
}
