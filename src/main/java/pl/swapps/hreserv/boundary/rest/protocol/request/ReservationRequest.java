package pl.swapps.hreserv.boundary.rest.protocol.request;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import pl.swapps.hreserv.validation.DatesInOrder;

import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@DatesInOrder(before = "dateFrom", after = "dateTo")
public class ReservationRequest {

    @NotNull
    private Long customerId;

    @NotNull
    @FutureOrPresent
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @ApiParam(example = "2019-03-15", required = true, format = "YYYY-MM-DD")
    private LocalDate dateFrom;

    @NotNull
    @Future
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @ApiParam(example = "2019-03-25", required = true, format = "YYYY-MM-DD")
    private LocalDate dateTo;
}
