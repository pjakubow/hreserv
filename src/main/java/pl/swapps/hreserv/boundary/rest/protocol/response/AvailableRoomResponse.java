package pl.swapps.hreserv.boundary.rest.protocol.response;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@Builder
@Getter
@ToString
@EqualsAndHashCode
public class AvailableRoomResponse {

    private Long id;
    private Long hotelId;
    private String hotelName;
    private String hotelCity;
    private String roomNumber;
    private BigDecimal dailyRoomPrice;
    private BigDecimal totalCost;
}
