package pl.swapps.hreserv.boundary.rest.mapper;

import pl.swapps.hreserv.boundary.rest.protocol.response.AvailableRoomResponse;
import pl.swapps.hreserv.domain.room.AvailableRoomDto;

public class AvailableRoomResponseMapper {

    public static AvailableRoomResponse map(AvailableRoomDto dto) {
        return AvailableRoomResponse.builder()
                .id(dto.getId())
                .hotelId(dto.getHotelId())
                .hotelCity(dto.getHotelCity())
                .hotelName(dto.getHotelName())
                .roomNumber(dto.getRoomNumber())
                .dailyRoomPrice(dto.getDailyRoomPrice())
                .totalCost(dto.getTotalCost())
                .build();
    }
}
