package pl.swapps.hreserv.boundary.rest.errors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.swapps.hreserv.boundary.rest.errors.ErrorResponseCreator.ErrorResponse;
import pl.swapps.hreserv.boundary.rest.errors.ErrorResponseCreator.ValidationErrorResponse;
import pl.swapps.hreserv.exception.HReservException;

@Slf4j
@ControllerAdvice
public class HReservErrorHandler {

    @ExceptionHandler(HReservException.class)
    public ResponseEntity<ErrorResponse> handleApplicationError(HReservException ex) {
        log.warn("Handling application exception: {} - {}", ex.getClass().getSimpleName(), ex.getMessage());
        return ResponseEntity.badRequest().body(ErrorResponseCreator.create(ex));
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<ValidationErrorResponse> handleValidationError(BindException ex) {
        return this.handleValidationError(ex.getBindingResult(), ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationErrorResponse> handleValidationError(MethodArgumentNotValidException ex) {
        return this.handleValidationError(ex.getBindingResult(), ex.getMessage());
    }

    private ResponseEntity<ValidationErrorResponse> handleValidationError(BindingResult bindingResult, String message) {
        log.info("Handling validation error: {}", message);
        return ResponseEntity.badRequest().body(ErrorResponseCreator.create(bindingResult));
    }
}
