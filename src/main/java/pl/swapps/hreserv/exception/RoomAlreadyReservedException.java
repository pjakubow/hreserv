package pl.swapps.hreserv.exception;

import java.time.LocalDate;

public class RoomAlreadyReservedException extends HReservException {

    public RoomAlreadyReservedException(Long roomId, LocalDate dateFrom, LocalDate dateTo) {
        super(ErrorCodes.ROOM_UNAVAILABLE, "Could not make reservation for room " + roomId + " on " + dateFrom + " - " + dateTo);
    }
}
