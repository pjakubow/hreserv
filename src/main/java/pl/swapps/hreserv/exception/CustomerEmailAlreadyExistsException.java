package pl.swapps.hreserv.exception;

public class CustomerEmailAlreadyExistsException extends HReservException {

    public CustomerEmailAlreadyExistsException(String email) {
        super(ErrorCodes.CUSTOMER_EMAIL_ALREADY_EXISTS, "Customer with email: " + email + " has been already registered");
    }
}
