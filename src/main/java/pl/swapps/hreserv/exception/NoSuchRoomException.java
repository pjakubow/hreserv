package pl.swapps.hreserv.exception;

public class NoSuchRoomException extends HReservException {

    public NoSuchRoomException(Long roomId) {
        super(ErrorCodes.NO_SUCH_ROOM, "Could not find room with id: " + roomId);
    }
}
