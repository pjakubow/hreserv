package pl.swapps.hreserv.exception;

public class NoSuchCustomerException extends HReservException {

    public NoSuchCustomerException(Long customerId) {
        super(ErrorCodes.NO_SUCH_CUSTOMER, "Could not find customer with id: " + customerId);
    }
}
