package pl.swapps.hreserv.exception;

import lombok.Getter;

public class HReservException extends RuntimeException {

    @Getter
    private int code;

    public HReservException(int code, String message) {
        super(message);
        this.code = code;
    }
}
