package pl.swapps.hreserv.exception;

public class ErrorCodes {

    public static final int VALIDATION_ERROR = 1000;

    public static final int NO_SUCH_ROOM = 2001;

    public static final int NO_SUCH_CUSTOMER = 3001;
    public static final int CUSTOMER_EMAIL_ALREADY_EXISTS = 3002;

    public static final int ROOM_UNAVAILABLE = 4001;
    public static final int CUSTOMER_UNAVAILABLE = 4002;

    public static final int NO_SUCH_RESERVATION = 5001;

    public static final int UNAUTHORIZED_OPERATION = 6001;
}
