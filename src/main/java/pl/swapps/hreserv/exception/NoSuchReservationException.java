package pl.swapps.hreserv.exception;

public class NoSuchReservationException extends HReservException {

    public NoSuchReservationException(Long reservationId) {
        super(ErrorCodes.NO_SUCH_RESERVATION, "Could not find reservation with id: " + reservationId);
    }
}
