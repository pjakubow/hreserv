package pl.swapps.hreserv.exception;

public class UnauthorizedOperationException extends HReservException {

    public UnauthorizedOperationException(String message) {
        super(ErrorCodes.UNAUTHORIZED_OPERATION, message);
    }
}
