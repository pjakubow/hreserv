package pl.swapps.hreserv.exception;

import java.time.LocalDate;

public class CustomerAlreadyReservedException extends HReservException {

    public CustomerAlreadyReservedException(Long customerId, LocalDate dateFrom, LocalDate dateTo) {
        super(ErrorCodes.CUSTOMER_UNAVAILABLE,
                "Could not make reservation for customer " + customerId + " on " + dateFrom + " - " + dateTo);
    }
}
