package pl.swapps.hreserv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HreservApplication {

    public static void main(String[] args) {
        SpringApplication.run(HreservApplication.class, args);
    }
}
