package pl.swapps.hreserv.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Optional;

public class DatesInOrderValidator implements ConstraintValidator<DatesInOrder, Object> {

    private static Logger log = LoggerFactory.getLogger(DatesInOrderValidator.class);

    private String beforeName;

    private String afterName;

    private String errorMessage;

    @Override
    public void initialize(DatesInOrder annotation) {
        beforeName = annotation.before();
        afterName = annotation.after();
        errorMessage = annotation.message();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext ctx) {
        boolean valid = false;

        try {
            final LocalDate before = getStringProperty(value, beforeName);
            final LocalDate after = getStringProperty(value, afterName);
            if (before != null && after != null) {
                valid = before.isBefore(after);
            }
        } catch (final Exception e) {
            log.error("Exception during date validation", e);
        }

        if (!valid) {
            ctx.disableDefaultConstraintViolation();
            ctx.buildConstraintViolationWithTemplate(errorMessage)
                    .addPropertyNode(afterName)
                    .addConstraintViolation();
        }

        return valid;
    }

    private LocalDate getStringProperty(Object obj, String name)
            throws IllegalAccessException, NoSuchMethodException, InvocationTargetException, DateTimeParseException {
        return Optional
                .ofNullable(obj.getClass().getDeclaredMethod("get" + name.substring(0, 1).toUpperCase() + name.substring(1)).invoke(obj))
                .map(Object::toString)
                .map(LocalDate::parse)
                .orElse(null);
    }
}
