# HRESERV

A recruitment assignment

## Building an application

```
./mvnw verify
```

## Running an application on localhost

##### With in-memory H2 database (built from scratch each time)

```
java -jar target/hreserv-0.0.1-SNAPSHOT.jar
```

##### With dockerized PostgreSQL

```
docker run --name hreserv-postgres -p 15432:5432 -e POSTGRES_PASSWORD=mysecretpassword -e POSTGRES_USER=hreserv -e POSTGRES_DB=hreserv -d postgres

java -Dspring.profiles.active=postgres -jar target/hreserv-0.0.1-SNAPSHOT.jar
```

## Running acceptance tests

##### Against a default instance (`http://localhost:8080`):

```
./mvnw -Dtest=HreservAcceptance test
```

##### Against a custom instance (e.g. heroku build: `https://hreserv.herokuapp.com`):

```
./mvnw -Dtest=HreservAcceptance test -DargLine="-Dhreserv.url=https://hreserv.herokuapp.com"
```